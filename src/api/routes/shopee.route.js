import express from 'express';
import validate from 'express-validation';
import middleware from '../middlewares/shopee.middleware';
import controller from '../controllers/v1/shopee.controller';

import {
    shopInfo
} from '../validations/shopee.validation';

const router = express.Router();

router
    .route('/auth')
    .get(
        controller.authLinkShopee
    );

router
    .route('/callback-link')
    .get(
        controller.callbackLink
    );

router
    .route('/shopee-sdk')
    .get(
        controller.shopeeSdk
    );

router
    .route('/cancel-auth')
    .get(
        controller.cancelAuth
    );

router
    .route('/shop-info')
    .get(
        validate(shopInfo),
        controller.getShopInfo
    );

router
    .route('/webhook')
    .post(
        middleware.prepareWebhook,
        controller.webhookAuth
    );

router
    .route('/webhook/:id')
    .get(
        middleware.load,
        controller.detail
    );

export default router;

