import express from 'express';
import { authorize } from 'auth-adapter';
import Permissions from '../../common/utils/Permissions';
import controller from '../controllers/v1/sync.controller';
import middleware from '../middlewares/sync.middleware';

const router = express.Router();

router
    .route('/')
    .get(
        middleware.count,
        controller.list
    )
    .post(
        authorize([Permissions.LOGGED_IN]),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        middleware.prepareUpdate,
        controller.update
    )
    .delete(
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        controller.delete
    );


export default router;

