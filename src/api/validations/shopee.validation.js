import Joi from 'joi';

module.exports = {
    shopInfo: {
        query: {
            // pagging
            shop_id: Joi.number()
                .required(),
            code: Joi.string()
                .allow(null, '')
        }
    }
};
