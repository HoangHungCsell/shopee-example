import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import Sync from '../../../common/models/sync.model';

/**
 * Create
 *
 * @public
 * @param body as Sync
 * @returns {Promise<Sync>, APIError>}
 */
exports.create = async (req, res, next) => {
    await Sync.create(
        req.body
    ).then(data => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: Sync.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Sync[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Sync.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(s => Sync.transform(s))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Sync>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Sync.transform(req.locals.sync) });

/**
 * Update
 *
 * @public
 * @param {String} id
 * @param {Sync} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { sync: data } = req.locals;
    // replace existing data
    return Sync.update(
        req.body,
        {
            where: {
                id: data.id
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {*} id
 * @returns {Promise<, APIError}
 */
exports.delete = async (req, res, next) => {
    const { sync: data } = req.locals;
    return Sync.update(
        {
            is_active: false,
            updated_at: new Date()
        },
        {
            where: {
                id: data.id,
                is_active: true
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.REMOVE_SUCCESS
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
