import httpStatus from 'http-status';
import { pick } from 'lodash';

import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';

import Sync from '../../common/models/sync.model';


/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Sync.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.sync = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Sync.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare sync params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = Sync.filterParams(req.body);
        params.created_by = pick(req.user, ['id', 'name']);
        // transform body
        req.body = params;

        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare sync update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { sync: oldModel } = req.locals;
        const params = Sync.filterParams(req.body);

        const dataChanged = Sync.getChangedProperties({
            oldModel,
            newModel: params
        });
        // transform body
        if (dataChanged.length === 0) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Bạn chưa thay đổi gì để cập nhật!'
            });
        }
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

