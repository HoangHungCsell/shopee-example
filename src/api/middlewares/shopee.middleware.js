import { handler as ErrorHandler } from './error';

import Provider from '../../common/models/provider.model';
import shopeeAdapter from '../../common/services/adapters/shopee-adapter';

/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const data = await Provider.get(req.params.id, 'shopee');
        req.locals = req.locals ? req.locals : {};
        req.locals.provider = Provider.transform(data);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

exports.prepareWebhook = async (req, res, next) => {
    try {
        const { code, shop_id } = req.body;
        const checkWebhook = await shopeeAdapter.checkWebhookShopee({
            body: req.body,
            auth: req.get('Authorization') || null
        });
        // check nếu shop auth giờ auth lại thì renew lại access_token
        // vì access_token cũ trong database còn có 5p
        if (checkWebhook && code === 1) {
            const provider = await Provider.checkCodeWebhook(shop_id, code, 'shopee');
            if (provider) {
                await Provider.update(
                    {
                        data: req.body,
                        updated_at: new Date()
                    },
                    {
                        where: {
                            'data.shop_id': shop_id,
                            'data.code': 1,
                            provider: 'shopee',
                            is_active: true
                        },
                        renew_token: true,
                        individualHooks: true
                    }
                );
            } else {
                await Provider.create({
                    data: req.body,
                    provider: 'shopee',
                    type: 'auth'
                });
            }
        }

        // check hủy auth
        if (checkWebhook && code === 2) {
            const provider = await Provider.checkCodeWebhook(shop_id, code, 'shopee');
            if (provider) {
                await Provider.update(
                    {
                        data: req.body,
                        updated_at: new Date()
                    },
                    {
                        where: {
                            'data.shop_id': shop_id,
                            'data.code': 2,
                            provider: 'shopee',
                            is_active: true
                        },
                        renew_token: true,
                        individualHooks: true
                    }
                );
            } else {
                await Provider.create({
                    data: req.body,
                    provider: 'shopee',
                    type: 'cancelAuth'
                });
            }
        }
        req.locals = req.locals ? req.locals : {};
        req.locals.checkWebhook = checkWebhook;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
