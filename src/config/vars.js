

// import .env variables
require('dotenv').config();

module.exports = {
    serviceName: 'cocolux_shopee_service',
    env: process.env.NODE_ENV,
    port: process.env.PORT || 8002,
    jwtIssuerPrefix: process.env.JWT_ISSUER_PREFIX,
    logs: process.env.NODE_ENV === 'production'
        ? 'combined'
        : 'dev',
    postgres: {
        uri: process.env.NODE_ENV === 'production'
            ? process.env.DATABASE_URL
            : process.env.DATABASE_URL
    },
    mongo: {
        uri:
            process.env.NODE_ENV === 'production'
                ? process.env.MONGO_URI
                : process.env.MONGO_URI_TEST
    },
    rabbit: {
        uri: process.env.RABBITMQ_URI
    },
    redis: {
        uri: process.env.REDIS_URI
    },
    otherServices: {
        manager: process.env.MANAGER_SERVICE_URL
    },
    thirdPartyServices: {
        shopeeAuth: {
            key: process.env.NODE_ENV === 'production'
                ? '19e62fd995b808734382351321ba8e52ad1103e24d28bf87f7b858b3edfe8cec'
                : '19e62fd995b808734382351321ba8e52ad1103e24d28bf87f7b858b3edfe8cec',
            partnerId: process.env.NODE_ENV === 'production'
                ? 1003072
                : 1003072,
        },
        ghnAuth: {
            appId: process.env.NODE_ENV === 'production'
                ? '1371000'
                : '75894',
            appToken: process.env.NODE_ENV === 'production'
                ? 'ed9c78cd-2d74-11eb-b36a-0e2790f48b9c'
                : '911e3179-1d82-11eb-a0d5-8a8a2026b1b4'
        },
        ghtkAuth: {
            appToken: process.env.NODE_ENV === 'production'
                ? 'B184B27B1Cf47b44f79fd062E6e3663ea009587c'
                : 'B184B27B1Cf47b44f79fd062E6e3663ea009587c'
        },
        ahmAuth: {
            appId: process.env.NODE_ENV === 'production'
                ? '2500622'
                : '2500622',
            appToken: process.env.NODE_ENV === 'production'
                ? '911e3179-1d82-11eb-a0d5-8a8a2026b1b4'
                : '911e3179-1d82-11eb-a0d5-8a8a2026b1b4'
        },
    }
};
