/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { pick, isEqual, isUndefined, isNil, omitBy } from 'lodash';
import moment from 'moment-timezone';

import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';
import eventBus from '../services/event-bus';


/**
 * Create connection
 */
// const { postgres } = Sequelize;
const { sequelize } = postgres;
class Provider extends Model { }

const PUBLIC_FIELDS = [];

/**
 * Provider Schema
 * @public
 */
Provider.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        data: {
            type: DataTypes.JSONB,
            allowNull: false
        },
        provider: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        type: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        // manager
        is_active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        created_by: {
            type: DataTypes.JSONB,
            defaultValue: null // id | name
        }
    },
    {
        timestamps: false,
        sequelize,
        modelName: 'provider',
        tableName: 'tbl_providers'
    }
);

/**
 * Register event emiter
 */
Provider.Events = {
    PROVIDER_CREATED: `${serviceName}.provider.created`,
    PROVIDER_UPDATED: `${serviceName}.provider.updated`,
    PROVIDER_DELETED: `${serviceName}.provider.deleted`,
};
Provider.EVENT_SOURCE = `${serviceName}.provider`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Provider.addHook('beforeCreate', async () => {
});

Provider.addHook('afterCreate', (data) => {
    const provider = data.dataValues;
    if (provider.data.code === 2) {
        provider.renew_token = true;
        eventBus.emit(Provider.Events.PROVIDER_CREATED, provider);
    }
});

Provider.addHook('afterUpdate', (data, options) => {
    const { renew_token } = options;
    const provider = data.dataValues;
    provider.renew_token = renew_token;
    eventBus.emit(Provider.Events.PROVIDER_UPDATED, provider);
});


/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
// function checkMinMaxOfConditionFields(options, field, type = 'Number') {
//     let _min = null;
//     let _max = null;

//     // Transform min max
//     if (type === 'Date') {
//         _min = new Date(options[`min_${field}`]);
//         _min.setHours(0, 0, 0, 0);

//         _max = new Date(options[`max_${field}`]);
//         _max.setHours(23, 59, 59, 999);
//     } else {
//         _min = parseFloat(options[`min_${field}`]);
//         _max = parseFloat(options[`max_${field}`]);
//     }

//     // Transform condition
//     if (
//         !isNil(options[`min_${field}`]) ||
//         !isNil(options[`max_${field}`])
//     ) {
//         if (
//             options[`min_${field}`] &&
//             !options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.gte]: _min
//             };
//         } else if (
//             !options[`min_${field}`] &&
//             options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.lte]: _max
//             };
//         } else {
//             options[field] = {
//                 [Op.gte]: _min || 0,
//                 [Op.lte]: _max || 0
//             };
//         }
//     }

//     // Remove first condition
//     delete options[`max_${field}`];
//     delete options[`min_${field}`];
// }

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    if (options.types) {
        options.type = {
            [Op.in]: options.types.split(',')
        };
    }
    delete options.types;

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        case 'total_price':
            sort = ['total_price', order_by];
            break;
        default: sort = ['id', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Provider.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'data',
        'provider',
        'type',
        'is_active',
        'created_at',
        'updated_at',
        'created_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Provider.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'note',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (
            !isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Provider.get = async (id, source) => {
    try {
        const provider = await Provider.findOne({
            where: {
                'data.shop_id': id,
                provider: source,
                is_active: true
            }
        });
        if (!provider) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy thông tin shop shopee !'
            });
        }
        return provider;
    } catch (ex) {
        throw ex;
    }
};

Provider.checkCodeWebhook = async (shopId, code, provider) => {
    try {
        const providerCheckCode = await Provider.findOne({
            where: {
                'data.shop_id': shopId,
                'data.code': code,
                provider,
                is_active: true
            }
        });
        return providerCheckCode;
    } catch (ex) {
        throw ex;
    }
};

/**
 * Get list order
 *
 * @public
 * @param {Parameters} params
 */
Provider.list = async ({
    min_created_at,
    max_created_at,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        min_created_at,
        max_created_at
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Provider.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total quantity items list records
 *
 * @public
 * @param {Parameters} params
 */
Provider.totalRecords = async ({
    min_created_at,
    max_created_at
}) => {
    try {
        const options = filterConditions({
            min_created_at,
            max_created_at
        });

        return Provider.count({
            where: options
        });
    } catch (ex) {
        throw ex;
    }
};


/**
 * Filter only allowed fields from order
 *
 * @param {Object} params
 */
Provider.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Provider
 */
export default Provider;
