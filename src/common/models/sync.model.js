/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes } from 'sequelize';
import { pick, isEqual, isUndefined, isNil, omitBy } from 'lodash';
import moment from 'moment-timezone';

import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';


/**
 * Create connection
 */
// const { postgres } = Sequelize;
const { sequelize } = postgres;
class Sync extends Model { }

const PUBLIC_FIELDS = [
    'provider',
    'merchant',
    'store',
    'sync_order',
    'confirm_delivery_return',
    'sync_sale_quantity',
    'formula_sale_quantity',
    'sync_price',
    'formula_price_book',
    'sync_sku',
    'remove_sync_sku'
];

Sync.SaleQuantity = {
    TK: 1, // tồn kho
    TKTDH: 2 // tồn kho trừ đặt hàng
};
/**
 * Sync Schema
 * @public
 */
Sync.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        provider: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        merchant: {
            type: DataTypes.JSONB,
            defaultValue: null // id(id của shop hoặc main_account), name (shop_id, main_account_id)
        },
        store: {
            type: DataTypes.JSONB,
            allowNull: false
        },
        sync_order: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        confirm_delivery_return: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        sync_sale_quantity: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        formula_sale_quantity: {
            type: DataTypes.INTEGER,
            defaultValue: null
        },
        sync_price: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        formula_price_book: {
            type: DataTypes.JSONB,
            defaultValue: null
        },
        sync_sku: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        remove_sync_sku: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        product_connected: {
            type: DataTypes.DECIMAL,
            defaultValue: 0
        },
        // manager
        is_active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        device_id: {
            type: DataTypes.STRING(255),
            defaultValue: 'unkown'
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        created_by: {
            type: DataTypes.JSONB,
            defaultValue: null // id | name
        }
    },
    {
        timestamps: false,
        sequelize,
        modelName: 'sync',
        tableName: 'tbl_syncs'
    }
);


/**
 * Register event emiter
 */
Sync.Events = {
};
Sync.EVENT_SOURCE = `${serviceName}.order`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Sync.addHook('beforeCreate', async () => {
});

Sync.addHook('afterCreate', () => {
});

Sync.addHook('afterUpdate', () => {
});


/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
// function checkMinMaxOfConditionFields(options, field, type = 'Number') {
//     let _min = null;
//     let _max = null;

//     // Transform min max
//     if (type === 'Date') {
//         _min = new Date(options[`min_${field}`]);
//         _min.setHours(0, 0, 0, 0);

//         _max = new Date(options[`max_${field}`]);
//         _max.setHours(23, 59, 59, 999);
//     } else {
//         _min = parseFloat(options[`min_${field}`]);
//         _max = parseFloat(options[`max_${field}`]);
//     }

//     // Transform condition
//     if (
//         !isNil(options[`min_${field}`]) ||
//         !isNil(options[`max_${field}`])
//     ) {
//         if (
//             options[`min_${field}`] &&
//             !options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.gte]: _min
//             };
//         } else if (
//             !options[`min_${field}`] &&
//             options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.lte]: _max
//             };
//         } else {
//             options[field] = {
//                 [Op.gte]: _min || 0,
//                 [Op.lte]: _max || 0
//             };
//         }
//     }

//     // Remove first condition
//     delete options[`max_${field}`];
//     delete options[`min_${field}`];
// }

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;
    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default: sort = ['id', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Sync.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'provider',
        'merchant',
        'store',
        'sync_order',
        'confirm_delivery_return',
        'sync_sale_quantity',
        'formula_sale_quantity',
        'sync_price',
        'formula_price_book',
        'sync_sku',
        'remove_sync_sku',
        'product_connected',
        'is_active',
        'device_id',
        'created_by',
        'created_at',
        'updated_at',
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe decimal
    const decimalFields = [
        'product_connected'
    ];
    decimalFields.forEach((field) => {
        transformed[field] = parseInt(params[field], 0);
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Sync.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'provider',
        'merchant',
        'store',
        'sync_order',
        'confirm_delivery_return',
        'sync_sale_quantity',
        'formula_sale_quantity',
        'sync_price',
        'formula_price_book',
        'sync_sku',
        'remove_sync_sku'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (
            !isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Sync.get = async (id) => {
    try {
        const sync = await Sync.findOne({
            where: {
                id,
                is_active: true
            }
        });
        if (!sync) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy đơn hàng !'
            });
        }
        return sync;
    } catch (ex) {
        throw ex;
    }
};

/**
 * Get list order
 *
 * @public
 * @param {Parameters} params
 */
Sync.list = async ({
    min_created_at,
    max_created_at,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        min_created_at,
        max_created_at
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Sync.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total quantity items list records
 *
 * @public
 * @param {Parameters} params
 */
Sync.totalRecords = async ({
    min_created_at,
    max_created_at
}) => {
    try {
        const options = filterConditions({
            min_created_at,
            max_created_at
        });

        return Sync.count({
            where: options
        });
    } catch (ex) {
        throw ex;
    }
};


/**
 * Filter only allowed fields from order
 *
 * @param {Object} params
 */
Sync.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Sync
 */
export default Sync;
