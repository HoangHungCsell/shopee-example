/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isUndefined, isNil, omitBy } from 'lodash';
import moment from 'moment-timezone';

import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';


/**
 * Create connection
 */
// const { postgres } = Sequelize;
const { sequelize } = postgres;
class Token extends Model { }

/**
 * Token Schema
 * @public
 */
Token.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        data: {
            type: DataTypes.JSONB,
            allowNull: false
        },
        provider: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        merchant: {
            type: DataTypes.JSONB,
            defaultValue: null // id(id của shop hoặc main_account), name (shop_id, main_account_id)
        },
        // manager
        is_active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        }
    },
    {
        timestamps: false,
        sequelize,
        modelName: 'token',
        tableName: 'tbl_tokens'
    }
);


/**
 * Register event emiter
 */
Token.Events = {
};
Token.EVENT_SOURCE = `${serviceName}.order`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Token.addHook('beforeCreate', async () => {
});

Token.addHook('afterCreate', () => {
});

Token.addHook('afterUpdate', () => {
});


/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
// function checkMinMaxOfConditionFields(options, field, type = 'Number') {
//     let _min = null;
//     let _max = null;

//     // Transform min max
//     if (type === 'Date') {
//         _min = new Date(options[`min_${field}`]);
//         _min.setHours(0, 0, 0, 0);

//         _max = new Date(options[`max_${field}`]);
//         _max.setHours(23, 59, 59, 999);
//     } else {
//         _min = parseFloat(options[`min_${field}`]);
//         _max = parseFloat(options[`max_${field}`]);
//     }

//     // Transform condition
//     if (
//         !isNil(options[`min_${field}`]) ||
//         !isNil(options[`max_${field}`])
//     ) {
//         if (
//             options[`min_${field}`] &&
//             !options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.gte]: _min
//             };
//         } else if (
//             !options[`min_${field}`] &&
//             options[`max_${field}`]
//         ) {
//             options[field] = {
//                 [Op.lte]: _max
//             };
//         } else {
//             options[field] = {
//                 [Op.gte]: _min || 0,
//                 [Op.lte]: _max || 0
//             };
//         }
//     }

//     // Remove first condition
//     delete options[`max_${field}`];
//     delete options[`min_${field}`];
// }

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    if (options.types) {
        options.type = {
            [Op.in]: options.types.split(',')
        };
    }
    delete options.types;

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        case 'total_price':
            sort = ['total_price', order_by];
            break;
        default: sort = ['id', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Token.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'data',
        'is_active',
        'created_at',
        'updated_at',
        'created_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Token.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'note',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (
            !isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Token.get = async (id, source) => {
    try {
        const token = await Token.findOne({
            where: {
                'merchant.id': id,
                provider: source,
                is_active: true
            }
        });
        if (!token) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy thông tin shop shopee !'
            });
        }
        return token;
    } catch (ex) {
        throw ex;
    }
};

/**
 * Get list order
 *
 * @public
 * @param {Parameters} params
 */
Token.list = async ({
    min_created_at,
    max_created_at,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        min_created_at,
        max_created_at
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Token.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total quantity items list records
 *
 * @public
 * @param {Parameters} params
 */
Token.totalRecords = async ({
    min_created_at,
    max_created_at
}) => {
    try {
        const options = filterConditions({
            min_created_at,
            max_created_at
        });

        return Token.count({
            where: options
        });
    } catch (ex) {
        throw ex;
    }
};

/**
 * @typedef Token
 */
export default Token;
