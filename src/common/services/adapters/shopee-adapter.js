import { createHmac } from 'crypto';
import axios from 'axios';
import { env, thirdPartyServices } from '../../../config/vars';
import Token from '../../models/token.model';
// import Provider from '../../models/provider.model';

const {
    shopeeAuth: {
        key,
        partnerId
    }
} = thirdPartyServices;

const BASE_URL = env === 'production'
    ? 'https://partner.shopeemobile.com'
    : 'https://partner.test-stable.shopeemobile.com';

// const timestamp = Math.floor(new Date().getTime() / 1000);

// tạo link auth
async function authLinkShopee() {
    try {
        const apiPath = '/api/v2/shop/auth_partner';
        const timestamp = Math.floor(new Date().getTime() / 1000);
        const requestUrl = `${BASE_URL}${apiPath}`;
        const redirectUrl = 'https://csell-shopee-example.herokuapp.com/v1/shopee/callback-link';
        const baseString = `${partnerId}${apiPath}${timestamp}`;
        const tokenBaseString = createHmac('sha256', key).update(baseString).digest('hex');
        const authLink = `${requestUrl}?partner_id=${partnerId}&redirect=${redirectUrl}&sign=${tokenBaseString}&timestamp=${timestamp}`;
        return authLink;
    } catch (error) {
        return error;
    }
}

// hủy auth
async function cancelAuth() {
    try {
        const apiPath = '/api/v2/shop/cancel_auth_partner';
        const timestamp = Math.floor(new Date().getTime() / 1000);
        const requestUrl = `${BASE_URL}${apiPath}`;
        const redirectUrl = 'https://api-cocolux.csell.com.vn/v1/orders';
        const baseString = `${partnerId}${apiPath}${timestamp}`;
        const tokenBaseString = createHmac('sha256', key).update(baseString).digest('hex');
        const authLink = `${requestUrl}?partner_id=${partnerId}&redirect=${redirectUrl}&sign=${tokenBaseString}&timestamp=${timestamp}`;
        return authLink;
    } catch (error) {
        return error;
    }
}

/**
* Calculate Shipping Fee
* @param {*} options
*/
async function checkWebhookShopee({
    body,
    auth
}) {
    try {
        // url webhook của mình
        const webhookUrl = env === 'production'
            ? 'https://csell-shopee-example.herokuapp.com/v1/shopee/webhook'
            : 'https://csell-shopee-example.herokuapp.com/v1/shopee/webhook';
        if (!body || body.code === 0) {
            return true;
        }
        const character = '|';
        const baseString = `${webhookUrl}${character}${JSON.stringify(body)}`;
        const signature = createHmac('sha256', key).update(baseString).digest('hex');
        return signature === auth;
    } catch (error) {
        return error;
    }
}

async function getAccessTokenShopee({
    code,
    shopId = 0,
    mainAccountId = 0
}) {
    try {
        const timestamp = Math.floor(new Date().getTime() / 1000);
        const apiPath = '/api/v2/auth/token/get';
        const requestUrl = `${BASE_URL}${apiPath}`;
        const baseString = `${partnerId}${apiPath}${timestamp}`;
        const sign = createHmac('sha256', key).update(baseString).digest('hex');
        const params = {
            sign,
            partner_id: partnerId,
            timestamp
        };
        const body = {
            code,
            partner_id: partnerId,
            // shop_id: shopId,
            // main_account_id: mainAccountId
        };
        if (shopId) {
            body.shop_id = shopId;
        }
        if (mainAccountId) {
            body.main_account_id = mainAccountId;
        }
        const response = await axios.post(
            requestUrl,
            body,
            {
                params
            }
        );
        if (response && response.data && response.data.access_token) {
            await Token.create({
                data: response.data,
                provider: 'shopee',
                merchant: {
                    id: shopId || mainAccountId,
                    name: shopId ? 'shop_id' : 'main_account_id'
                }
            });
        }
        return response.data;
    } catch (error) {
        return error.response.data;
    }
}

async function getRefreshTokenShopee({
    data,
    shopId = 0,
    mainAccountId = 0
}) {
    try {
        const timestamp = Math.floor(new Date().getTime() / 1000);
        const apiPath = '/api/v2/auth/access_token/get';
        const requestUrl = `${BASE_URL}${apiPath}`;
        const baseString = `${partnerId}${apiPath}${timestamp}`;
        const sign = createHmac('sha256', key).update(baseString).digest('hex');
        const params = {
            sign,
            partner_id: partnerId,
            timestamp
        };
        const body = {
            refresh_token: data.refresh_token,
            partner_id: partnerId,
            // shop_id: shopId,
            // main_account_id: mainAccountId
        };
        if (shopId) {
            body.shop_id = shopId;
        }
        if (mainAccountId) {
            body.main_account_id = mainAccountId;
        }
        console.log('bodyyyyyyyy', body);
        const response = await axios.post(
            requestUrl,
            body,
            {
                params
            }
        );
        if (response && response.data && response.data.access_token) {
            await Token.update(
                {
                    data: response.data,
                    updated_at: new Date()
                },
                {
                    where: {
                        'merchant.id': shopId || mainAccountId,
                        is_active: true,
                        provider: 'shopee'
                    }
                }
            );
            return response.data;
        }
        return false;
    } catch (error) {
        return error.response.data;
    }
}

// lấy token trong database của mình nếu đã có
async function getToken(id) {
    try {
        const dataToken = await Token.findOne({
            where: {
                'merchant.id': id,
                is_active: true,
                provider: 'shopee'
            }
        });
        if (!dataToken) {
            return false;
        }
        const token = dataToken.dataValues;
        const mathNow = Math.ceil(new Date().getTime() / 1000);
        const mathUpdated = Math.ceil(token.updated_at.getTime() / 1000);
        const mathCreated = Math.ceil(token.created_at.getTime() / 1000);
        // check nếu access_token chưa hết hạn
        if (token && (mathNow - mathUpdated) < token.data.expire_in // ~4h
        ) {
            return token.data;
        }
        // check nếu access_token hết hạn nhưng refresh_token chưa hết hạn
        if (token
            && (mathNow - mathUpdated) > token.data.expire_in // ~4h
            && (mathNow - mathCreated) < 2592000 // 30 ngày
        ) {
            const refreshToken = await getRefreshTokenShopee({
                data: token.data,
                shopId: id
            });
            return refreshToken;
        }
        // check nếu cả access và refresh đều hết hạn
        if (
            token
            && (mathNow - mathCreated) > 2592000
        ) {
            await Token.update(
                {
                    is_active: false,
                    updated_at: new Date()
                },
                {
                    where: {
                        'merchant.id': id,
                        is_active: true,
                        provider: 'shopee'
                    }
                }
            );
            return false;
        }
        return false;
    } catch (error) {
        return error.response.data;
    }
}

async function generateSign({
    apiPath,
    accessToken,
    shopId,
    timestamp
}) {
    try {
        const baseString = `${partnerId}${apiPath}${timestamp}${accessToken}${shopId}`;
        const sign = createHmac('sha256', key).update(baseString).digest('hex');
        return sign;
    } catch (error) {
        return error;
    }
}

async function getShopInfo({
    shopId,
    code
}) {
    try {
        const timestamp = Math.floor(new Date().getTime() / 1000);
        const apiPath = '/api/v2/shop/get_shop_info';
        const requestUrl = `${BASE_URL}${apiPath}`;
        let token = await getToken(shopId);
        if (!token) {
            token = await getAccessTokenShopee({
                code,
                shopId: shopId
            });
        }
        // check nếu không có access_token (ví dụ như thiếu params code sẽ ko generate access_token)
        if (token && !token.access_token) {
            return token;
        }
        const sign = await generateSign({
            apiPath,
            accessToken: token.access_token,
            shopId,
            timestamp
        });
        const params = {
            partner_id: partnerId,
            timestamp,
            access_token: token.access_token,
            shop_id: shopId,
            sign
        };
        const response = await axios.get(
            requestUrl,
            {
                params
            }
        );
        return response.data;
    } catch (error) {
        return error.response.data;
    }
}

module.exports = {
    authLinkShopee,
    cancelAuth,
    checkWebhookShopee,
    getShopInfo
};
