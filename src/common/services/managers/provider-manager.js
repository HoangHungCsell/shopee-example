import eventBus from '../event-bus';
import Provider from '../../models/provider.model';
import Token from '../../models/token.model';

function registerProviderEvent() {
    /**
     * Register created event
     */
    eventBus.on(Provider.Events.PROVIDER_CREATED, async (data) => {
        try {
            if (data.renew_token) {
                await Token.update(
                    {
                        is_active: false,
                        updated_at: new Date()
                    },
                    {
                        where: {
                            'merchant.id': data.data.shop_id,
                            provider: 'shopee',
                            is_active: true
                        }
                    }
                );
            }
        } catch (error) {
            console.log('cannot save event', error);
        }
    });
    /**
     * Register updated event
     */
    eventBus.on(Provider.Events.PROVIDER_UPDATED, async (data) => {
        try {
            if (data.renew_token) {
                await Token.update(
                    {
                        is_active: false,
                        updated_at: new Date()
                    },
                    {
                        where: {
                            'merchant.id': data.data.shop_id,
                            provider: 'shopee',
                            is_active: true
                        }
                    }
                );
            }
        } catch (error) {
            console.log('cannot save event', error);
        }
    });
}

module.exports = {
    registerProviderEvent
};
