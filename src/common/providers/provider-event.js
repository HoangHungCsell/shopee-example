import providerEvent from '../services/managers/provider-manager';

export default {
    register: () => {
        providerEvent.registerProviderEvent();
    }
};
