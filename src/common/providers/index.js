import eventSource from './event-source';
import providerPriceEvent from './provider-event';

export default {
    register: () => {
        // register any event emitter || event rabbitmq here
        eventSource.register();
        providerPriceEvent.register();
    }
};
